# Supported tags

- `3.5.0-jdk-8`, `3.5.0`, `3.5-jdk-8`, `3.5`, `3-jdk-8`, `3`, `latest` [(maven-3.5.0/jdk-8/Dockerfile)](https://bitbucket.org/pandera-systems/maven-awsebcli/src/master/maven-3.5.0/jdk-8/Dockerfile)
- `3.5.0-jdk-8-alpine`, `3.5.0-alpine`, `3.5-jdk-8-alpine`, `3.5-alpine`, `3-jdk-8-alpine`, `3-alpine`, `alpine` [(maven-3.5.0/jdk-8/alpine/Dockerfile)](https://bitbucket.org/pandera-systems/maven-awsebcli/src/master/maven-3.5.0/jdk-8/alpine/Dockerfile)
- `1.0.0` *DEPRECATED*

# Quick Reference

-	**Maintained by**:  
	[Pandera Systems, LLC.](http://panderasystems.com/)

- **Where to file issues:**
  [BitBucket](https://bitbucket.org/pandera-systems/maven-awsebcli/issues)
